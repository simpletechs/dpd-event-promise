(function(Script, when) {
  var _run = Script.prototype.run;
  Script.prototype.run = function(ctx, domain, fn) {
    if (typeof domain === 'object') {
      domain.Promise = when;
    }
    _run.call(this, ctx, domain, fn);
  };
})(require('deployd/lib/script'), require('when'));
